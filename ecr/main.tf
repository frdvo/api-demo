module "ecr" {
  source                     = "cloudposse/ecr/aws"
  version                    = "0.32.3"
  name                       = var.container_name
  tags                       = local.workspace.tags
  principals_full_access     = local.workspace.principals_full_access
  principals_readonly_access = local.workspace.principals_readonly_access

}