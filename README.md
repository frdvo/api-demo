# Api-Demo

API-Demo is a project to show DevOps practices on Automation with 3 Musketeers, IaC with Terraform, multi-account AWS deployment with managed identity, Gitlab runners,  container build optimization and a simple API written in Python as workload. The intent of this demo is show good practices in DevOps and Cloud.
## Quick Local Deployment Instructions

Instructions to test the code and quickly deploy using a local environment with a local Terraform backend. It is a simplification from the original project to allow a quick assessment. 

### Prepare Your Environment

1. Create a programmatic access AWS IAM user with Administrator Access
2. Configure the default AWS profile with this user

If you have AWS CLI

`aws configure`

and type the credentials

If you don't have open `~/.aws/credentials` in your text editor and add:

````
[default]
aws_access_key_id = 
aws_secret_access_key =
````

Completing your credentials.

3. Clone the repo

    `git clone https://gitlab.com/frdvo/api-demo.git`

1. Change to the demo-local-backend branch
   
    `git checkout demo-local-backend`

### Deployment

1. Terraform Apply  ECR - Our Container Registry

    `FOLDER=ecr WORKSPACE=demo-local make apply`

2. Login Docker to ECR
   
   `make login`

3. Build the container
   
   `make build`

4. Test the container locally
   
   `make test`

5. Push the container to ECR
   
   `make push`

6. Terraform Plan to ECS Fargate

    `FOLDER=fargate WORKSPACE=demo-local make plan`

7. Terraform Plan to ECS Fargate

    `FOLDER=fargate WORKSPACE=demo-local make apply`

    Check the output for the test URL. If you want to change anything look after the workspace `demo-local` in `fargate/locals.tf`

8. Terraform Desstroy ECS Fargate
 
   `FOLDER=fargate WORKSPACE=demo-local make destroy`
   
    The LB Log bucket will fail to destruct because it is not empty.

    To empty the bucket use:

    `aws s3 rm s3://<bucket name from terraform error>/ --recursive`

    If you don't have AWS cli locally use (Only available in the demo-local-backend branch):

    `LBS3=<bucket name from terraform error> make rms3`

    Now you can destroy the bucket with:

    `FOLDER=fargate WORKSPACE=demo-local make destroy`

9. Terraform Destroy ECR
    
    `FOLDER=ecr WORKSPACE=demo-local make apply`

## Monorepo Pattern

The project is using a Mororepo, with a folder for the app, configs and Terraform IaC. The folders contents are described below

`docker` contains the code regarding the API, including the Dockerfile

`init`  contains the AWS profiles to be used inside the runner with managed identity

`backend`  Terraform code to deploy Remote backend to AWS with S3 and DynamoDB backend lock

`ecr` Terraform code to deploy the container registry

`fargate` Terraform code to deploy the container to AWS ECS Fargate, using a load balancer and a dedicated VPC

`runner` Terraform code to deploy Gilab runner to a shared DevOps account, in an existing VPC, allowing cross-account infrastructure deployment

`root folder` contains the special files:

`.gitlab-ci.yml` Gitlab CI pipeline configs

`docker-compose-yml` Containerers with the dependencies to use in the 3 musketeers pattern

`envvars.yml` Variavel validation parameters for the 3 musketeers pattern

`Makefile` Terraform Centralized Backend Config and all local automation for the 3 musketeers pattern

## Docker App

The `docker` folder contains the Dockerfile to build the application container, in a multi-step approach to create a light container

The API code is located in `docker\app\main.py`. It is a simple Python FastAPI showing the git hash present in an environment variable.

Edit the file `docker\app\main.py` to change the app version.

To build the container use:

`make build`

To test the container locally use:

`make test`

## Terraform Code on Mororepo Pattern

Folders are utilized to store Terraform code for different parts of the project and all folders follow the same pattern.

`terraform.tf` contains the provider requirement and backend configuration. The main branch use Terraform Remote Backend with an empty S3 config `s3 {}`, the rest of the configuration is passed via Makefile using partial backend configuration. It allows the use of a centralized backend to deploy to several AWS accounts

`locals.tf` contains the parameter to deploy the resources into different accounts grouped by Terraform workspaces. Each workspace has its own state.

`variables.tf` contains variables that should be acquired from the environment

`main.tf` and other `.tf` files contain the modules and resources to deploy, using `local.workspace` parameters or variables.

## Terraform Workspaces + Locals.tf

Terraform Workspaces in local.tf represents a target environment for deployment. Each workspace can be deployed to different AWS Accounts, allowing reuse of the same code for different environments as dev/uat/prod

## Terraform 3 Muskteers Deployment Pattern

To deploy Terraform code from the repository subfolders, always check the local.tf file for the desired workspace and then use:

`FOLDER=<folder name> WORKSPACE=<workspace name> make command`

You can check all commands in the Makefile, below `Terraform 3 Musketeer Monorepo generic targets` THe most important commands are `plan`, `apply`, and `destroy`

## Note about Terraform Folders and Modules

Open source off the shelf Terraform modules are used to save time.

## Centralized Terraform Backend

The Makefile is in charge to use partial backend configurations to configure the backend in a centralized AWS account that could be different than the deployment accounts.

Usually, the backend is deployed in a shared-services or DevOps AWS account.

The folder `backend` contains code to deploy the remote backend. To use it ensure the `backend\local.tf` workspace GLOBAL is configured accordingly. Also, for the first deployment check-in `backend\terraform.tf` if the line with `backend s3 {}` is commented.

In the Makefile check if the block `# Initialise Terraform REMOTE BACKEND` is commented and `# Initialise Terraform LOCAL BACKEND` is not commented.

Use the following command to deploy the remote backend

`FOLDER=backend WORKSPACE=global make apply`

After the deployment edit the Makefile `# Backend Configuration` block accordingly, ensure `# Initialise Terraform LOCAL BACKEND` is commented, and `# Initialise Terraform REMOTE BACKEND` uncommented.

Uncomment `backend\terraform.tf` if the line with `backend s3 {}` and run the command again to transfer the local backend state file to the remote backend

`FOLDER=backend WORKSPACE=global make apply`

From now, double-check all other folders `backend\terraform.tf` for the uncommented  `backend s3 {}` line.

## ECR - Container Registry

The container registry is deployed from our local computer.
The ECR_ACCOUNT_ID and ECR_REGION should be configured in `Makefile` If you have to pull the containers from other AWS accounts configure the `principals_readonly_access` parameter in the workspaces in `ecr/locals.tf`  with the other accounts ID or ARNs from specific resources.

To deploy use

`FOLDER=ecr WORKSPACE=<The desired workspace from ecr/locals.tf> make apply`

To destroy use

`FOLDER=ecr WORKSPACE=<The desired workspace from ecr/locals.tf> make destroy`

To login to the container registry using:

`make login`

To push the images use:

`make push`

## Fargate - Our serverless computing engine

![Fargate simplified design](asssets/images/fargate_simplified_design.png)

ECS Fargate with an ALB and dedicated VPC is used to deploy the containers.

`fargate/locals.tf` contains all configurations to run our workloads.

The most relevant for now are:

`desired_count` Numbers of containers running the API distribute evenly across different AZs.

Enable `HTTPS`

Define `default_certificate_arn`

Use these Load Balancer variables:

   `lb_http_ports = local.lb_http_redirect_to_https`

   `lb_https_ports = local.lb_https_forward_to_http`

`HTTP` only:

  `lb_http_ports  = local.lb_http_redirect_to_https`

  `lb_https_ports = local.lb_https_forward_to_http`

The dedicated VPC is to simplify the process, but it can be removed from `fargate\main.tf` and replaced for an existing VPC.

To deploy use

`FOLDER=fargate WORKSPACE=<The desired workspace from fargate/locals.tf> make apply`

To destroy use

`FOLDER=fargate WORKSPACE=<The desired workspace from fargate/locals.tf> make destroy`

### Monitoring

The containers logs go to CloudWatch Log Group, and the ALB access logs go to S3. You can use Athena to query the ALB access logs [click here to learn more](https://docs.aws.amazon.com/athena/latest/ug/application-load-balancer-logs.html)

### More information

Reffer to the module [Github repository](https://github.com/cn-terraform/terraform-aws-ecs-fargate)

## Runner - Bonus

The `runner` folder contains code and modules to deploy a self-managed Gitlab runner with access to an instance profile containing roles to deploy the services in different accounts.

The `ini/aws/config` contains the AWS Profiles with the roles the runner can assume using the managed identity.

Everything is configured in this folder and modules.

Only Local Deployment.

To deploy use

`FOLDER=runner WORKSPACE=<The desired workspace from runner/locals.tf> make apply`

To destroy use

`FOLDER=runner WORKSPACE=<The desired workspace from runner/locals.tf> make destroy`

Before making this repository public the runner will be destroyed due to security reasons.

Also, in production, the gitlab-runner-primary-role must contain a policy with the minimum privilege.


# CI/CD Architecture


![CI/CD Architecture](asssets/images/cicd_architecture.png)

Two AWS Accounts

*demo-shared* - Shared account hosting Terraform Backend, ECR and, also an instance of the API-Demo in Fargate environment

*demo-dev* - Secondary account to deploy API-Demo in Fargate

*demo-others* - Other accounts may be used to deploy API-Demo in Fargate

# CI - Bonus

Gitlab CI is used for continuous integration

When a Merge Request is opened the pipeline runs:

If `docker` is modified it runs a `docker build`

If `fargate` is modified it runs a `terraform plan` to all accounts

After a merge in the main branch it runs:

`make build` to create the latest git hashtag and refresh the container, if it is necessary

`make push` to push the last container changes or the last tag to the registry

`terraform apply` to apply the latest changes to the `demo-shared` and get ready a manual trigger to deploy the changes in `demo-dev`

# Improvements

- Validate the variables in make build, push and  login targets
- Make a better connection between build, push, login and terraform targets
- Improve IAM role policies for the runner using minimum privilege
- Use ECR interface endpoints and S3 endpoints to avoid the need to transfer the containers using the NAT gateway 
- Gitlab CI save Terraform plan as an artifact and use it on Terraform apply
- Gitlab CI append Terraform plan output and Docker Build outputs to Merge Requests
- Protect main branch and setup code owners for MR approvals
- Improve diagrams
- Improve README.md
