locals {
  env = {
    global = {
      aws_profile     = "demo-shared"
      aws_region      = "ap-southeast-2"
      resource_prefix = "api-demo"
      workspace_details = {
        demo-shared = [722141136946]
        demo-dev    = [354334841216]
      }
      tags = {
        repository = "api-demo/backend"
        workspace  = terraform.workspace
      }
    }
  }

  workspace = local.env[terraform.workspace]
}
