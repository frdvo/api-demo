module "backend" {
  source = "github.com/cmdlabs/cmd-tf-aws-backend?ref=0.9.1"

  resource_prefix             = local.workspace.resource_prefix
  prevent_unencrypted_uploads = true
  all_workspaces_details      = []
  workspace_details           = local.workspace.workspace_details
  tags                        = local.workspace.tags
}
