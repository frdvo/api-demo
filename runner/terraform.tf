terraform {
  backend "s3" {} # Uncomment this line to use remote backend configured by Makefile
  required_providers {
    aws = {
      version = "3.73.0"
      source  = "hashicorp/aws"
    }
  }
}

provider "aws" {
  profile = local.workspace.aws_profile
  region  = local.workspace.aws_region
}
