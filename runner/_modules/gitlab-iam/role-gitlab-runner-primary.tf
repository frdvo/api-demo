
resource "aws_iam_role" "gitlab_runner_primary" {
  count                = var.create_gitlab_runner_primary_role == true ? 1 : 0
  name                 = "gitlab-runner-primary-role"
  description          = "Used by gitlab infra runners to coordinate and trigger terraform actions in other accounts"
  assume_role_policy   = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": { 
        "Service": "ec2.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_instance_profile" "gitlab_runner_primary" {
  count      = var.create_gitlab_runner_primary_role == true ? 1 : 0
  name       = "gitlab-runner-primary-role"
  role       = aws_iam_role.gitlab_runner_primary[0].name
}

resource "aws_iam_role_policy_attachment" "gitlab_runner_primary" {
  count      = var.create_gitlab_runner_primary_role == true ? 1 : 0
  role       = aws_iam_role.gitlab_runner_primary[0].name
  policy_arn = "arn:aws:iam::aws:policy/AdministratorAccess"
}

