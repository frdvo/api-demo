variable "account_id" {
  description = "AWS Account ID where gitlab runner lives"
  type        = string
}

variable "create_gitlab_runner_primary_role" {
    description = "Create gitlab runner primary role"
    type = bool 
}

variable "create_gitlab_runner_secondary_role" {
    description = "Create gitlab runner secondary role"
    type = bool 
}