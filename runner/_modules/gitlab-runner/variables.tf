variable "aws_region" {
  type        = string
  description = "Region in which resources are deployed"
}

variable "whitelist_cidr_block" {
  type        = list(string)
  description = "List CIDR blocks to be whitelisted"
  default     = ["10.0.0.0/8"]
}

variable "vpc_name" {
  type        = string
  description = "Name of the VPC to deploy the resource"
}


variable "subnet_tier" {
  type        = string
  description = "Tier name of the subnet to deploy the resource"
  default     = "application"
}

variable "instance_type" {
  type        = string
  description = "Instance type"
  default     = "t3.medium"
}

variable "volume_size" {
  type        = number
  description = "Storage size for the instance"
  default     = 30
}

variable "sg_ingress_ports" {
  type        = list(number)
  description = "List of availability zones"
  default     = []
}

variable "gitlab" {
  type = object({
    https_url       = string
    ssh_url         = string
    concurrent      = string
    runner_name     = string
    runner_tags     = string
    token_ssm_param = string
    executor        = string
    docker_image    = optional(string)
  })
  description = "GitLab runner configuration"
}

variable "timezone" {
  type        = string
  description = "Server time zone configuration"
  default     = "Australia/NSW"
}

variable "instance_ssm_key_path" {
  type        = string
  description = "Path of the AWS Systems Manager parameter holding the private key value."
  default     = ""
}


variable "tags" {
  type        = map(string)
  description = "Common tags for resources"
  default = {
    managed_by = "terraform",
  }
}

variable "autoscaling_group" {
  type = object({
    min_size         = number
    max_size         = number
    desired_capacity = number
  })
  description = "Autoscaling group configuration"
  default = {
    min_size         = 1
    max_size         = 10
    desired_capacity = 1
  }
}

variable "enable_autoscaling_schedule" {
  type        = bool
  description = "Wheter to enable autoscaling start and stop schedules. Timezone is UTC (other configuration is not supported yet with aws v3.45.0 https://github.com/hashicorp/terraform-provider-aws/issues/18098)"
  default     = false
}

variable "autoscaling_schedule_start" {
  type        = string
  description = <<EOF
  Unix cron syntax format schedule/recurrence to start instances. Timezone is `UTC`.
  Note: timezone configuration is not supported yet with aws provider v3.45.0.
  See: https://github.com/hashicorp/terraform-provider-aws/issues/18098
  EOF 
  default     = "00 21 * * MON-FRI" # GMT +10 Startup instances each weekday at 07:00 (UTC to Australia/Sydney)
}

variable "autoscaling_schedule_stop" {
  type        = string
  description = <<EOF
  Unix cron syntax format schedule/recurrence to stop instances. Timezone is `UTC`.
  Note: timezone configuration is not supported yet with aws provider v3.45.0.
  See: https://github.com/hashicorp/terraform-provider-aws/issues/18098
  EOF 
  default     = "30 09 * * MON-FRI" # GMT +10 Stop instances each weekday at 19:30 (UTC to Australia/Sydney)
}

variable "iam_instance_profile" {
  type        = string
  description = "Instance profile to attach to runner"
}

variable "kms_key_id" {
  type        = string
  description = "kms_key_id  arn"

}

variable "keypair_name" {
  type = string
  description = "keypar name"
}