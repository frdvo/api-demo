locals {
  gitlab = defaults(var.gitlab, {
    https_url       = "https://gitlab.com"
    ssh_url         = "gitlab.com"
    concurrent      = "5"
    runner_name     = "gitlab-runner"
    runner_tags     = "linux"
    token_ssm_param = "/gitlab/token"
    executor        = "shell"
    docker_image    = "alpine:latest"
  })
}

resource "aws_autoscaling_group" "gitlab_runner" {
  name_prefix            = lower("${local.gitlab.runner_name}-asg")
  min_size               = var.autoscaling_group.min_size
  max_size               = var.autoscaling_group.max_size
  desired_capacity       = var.autoscaling_group.desired_capacity
  vpc_zone_identifier = [data.aws_subnet.tier_a.id, data.aws_subnet.tier_b.id]

  enabled_metrics = ["GroupMinSize", "GroupMaxSize", "GroupDesiredCapacity", "GroupInServiceInstances", "GroupPendingInstances", "GroupStandbyInstances", "GroupTerminatingInstances", "GroupTotalInstances"]

  mixed_instances_policy {
    instances_distribution {
      on_demand_allocation_strategy            = "prioritized"
      on_demand_base_capacity                  = 0
      on_demand_percentage_above_base_capacity = 0
      spot_allocation_strategy                 = "lowest-price"
      spot_instance_pools                      = 10
      spot_max_price                           = ""
    }

    launch_template {
      launch_template_specification {
        launch_template_id = aws_launch_template.gitlab_runner.id
        version            = "$Latest"
      }
    }
  }

  tags = concat(
    [for tag_key, tag_value in var.tags : {
      "key" : tag_key,
      "value" : tag_value,
      "propagate_at_launch" : true
    } if tag_key != "Name"],
    [
      {
        "key"                 = "Name"
        "value"               = lower(local.gitlab.runner_name)
        "propagate_at_launch" = true
      }
    ]
  )
}


resource "aws_launch_template" "gitlab_runner" {
  name_prefix   = lower(local.gitlab.runner_name)
  image_id      = data.aws_ami.amazon_linux_2.id
  instance_type = var.instance_type

  user_data = base64encode(templatefile("${path.module}/templates/userdata.sh.tpl",
    {
      aws_region       = var.aws_region
      gitlab_https_url = local.gitlab.https_url
      gitlab_ssh_url   = local.gitlab.ssh_url
      concurrent       = local.gitlab.concurrent
      token_ssm_param  = local.gitlab.token_ssm_param
      runner_tags      = local.gitlab.runner_tags
      runner_name      = lower(local.gitlab.runner_name)
      executor         = local.gitlab.executor
      docker_image     = local.gitlab.docker_image
      timezone         = var.timezone
    }
    )
  )

  vpc_security_group_ids = [aws_security_group.gitlab_runner.id]

  key_name = var.keypair_name

  block_device_mappings {
    device_name = "/dev/xvda"
    ebs {
      volume_size           = var.volume_size
      volume_type           = "gp3"
      delete_on_termination = true
      encrypted             = true
      kms_key_id            = var.kms_key_id
    }
  }

  iam_instance_profile { arn = data.aws_iam_instance_profile.runner.arn }
  # iam_instance_profile { arn = aws_iam_instance_profile.gitlab_runner.arn }
  monitoring { enabled = true }
  lifecycle { create_before_destroy = true }
  metadata_options {
    http_endpoint               = "enabled"
    http_put_response_hop_limit = 3 # Required for 3 Musketeers Pattern
  }

  tag_specifications {
    resource_type = "volume"
    tags = merge(
      var.tags,
      { "Name" : lower(local.gitlab.runner_name) },
    )
  }

  tag_specifications {
    resource_type = "instance"
    tags = merge(
      var.tags,
      { "Name" : lower(local.gitlab.runner_name) },
    )
  }
}

resource "aws_autoscaling_schedule" "gitlab_runner_start" {
  count                  = var.enable_autoscaling_schedule ? 1 : 0
  scheduled_action_name  = lower("${local.gitlab.runner_name}-asg-start")
  autoscaling_group_name = aws_autoscaling_group.gitlab_runner.name
  recurrence             = var.autoscaling_schedule_start
  min_size               = var.autoscaling_group.min_size
  max_size               = var.autoscaling_group.max_size
  desired_capacity       = var.autoscaling_group.desired_capacity
}

resource "aws_autoscaling_schedule" "gitlab_runner_stop" {
  count                  = var.enable_autoscaling_schedule ? 1 : 0
  scheduled_action_name  = lower("${local.gitlab.runner_name}-asg-stop")
  autoscaling_group_name = aws_autoscaling_group.gitlab_runner.name
  recurrence             = var.autoscaling_schedule_stop
  min_size               = 0
  max_size               = 0
  desired_capacity       = 0
}
