#!/bin/bash -v
set -euo pipefail

export PATH=$PATH:/usr/local/bin

#-------------------------------------------------------------------------------
# Add and enable repos
#-------------------------------------------------------------------------------
# Official installation instructions from Gitlab: https://docs.gitlab.com/runner/install/linux-repository.html
curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.rpm.sh | sudo bash
amazon-linux-extras enable docker
# Install nodejs v12
curl -o nodesource-release-el7-1.noarch.rpm https://rpm.nodesource.com/pub_12.x/el/7/x86_64/nodesource-release-el7-1.noarch.rpm
rpm -i --nosignature --force nodesource-release-el7-1.noarch.rpm
# Update
yum update -y

#-------------------------------------------------------------------------------
# CloudWatch Agent configuration
#-------------------------------------------------------------------------------
# Download CloudWatch Agent on the EC2 instance
# https://docs.aws.amazon.com/AmazonCloudWatch/latest/monitoring/install-CloudWatch-Agent-on-EC2-Instance.html
# Install CloudWatch Agent on the EC2 instance
yum install -y amazon-cloudwatch-agent

# Configure CloudWatch Agent on the EC2 instance
CW_AGENT_CONFIG='/tmp/aws_ec2_cloudwatch.json'
# Note: Double dollar sign for metrics.append_dimensions.InstanceId 
# is required for terraform to process the template. 
# Escaping the first dollar sign is needed so that bash does not replace the value.
# The literal expresion is requried by CloudWatch agent.
cat > $CW_AGENT_CONFIG <<EOL
{
  "agent": {
    "metrics_collection_interval": 60
  },
  "logs": {
    "logs_collected": {
      "files": {
        "collect_list": [
          {
            "file_path": "/var/log/cron",
            "log_group_name": "linux/var-log-cron"
          },
          {
            "file_path": "/var/log/messages",
            "log_group_name": "linux/var-log-message"
          },
          {
            "file_path": "/var/log/secure",
            "log_group_name": "linux/var-log-secure"
          },
          {
            "file_path": "/var/log/cloud-init-output.log",
            "log_group_name": "linux/var-log-cloud-init-output"
          }
        ]
      }
    }
  },
  "metrics": {
    "namespace": "cw-agent-linux",
    "append_dimensions": {
      "InstanceId": "\$${aws:InstanceId}"
    },
    "aggregation_dimensions": [
      [
        "InstanceId"
      ]
    ],
    "metrics_collected": {
      "cpu": {
        "measurement": [
          "cpu_usage_idle",
          "cpu_usage_iowait",
          "cpu_usage_user",
          "cpu_usage_system"
        ],
        "resources": [
          "*"
        ],
        "totalcpu": false
      },
      "disk": {
        "measurement": [
          "used_percent",
          "inodes_free"
        ],
        "resources": [
          "*"
        ]
      },
      "diskio": {
        "measurement": [
          "io_time"
        ],
        "resources": [
          "*"
        ]
      },
      "mem": {
        "measurement": [
          "mem_used_percent"
        ]
      },
      "swap": {
        "measurement": [
          "swap_used_percent"
        ]
      }
    }
  }
}
EOL

# Start and reload CloudWatch configuration
/opt/aws/amazon-cloudwatch-agent/bin/amazon-cloudwatch-agent-ctl -a fetch-config -m ec2 -s -c file:$CW_AGENT_CONFIG

#-------------------------------------------------------------------------------
# Install and update requirements
#-------------------------------------------------------------------------------
# Note: docker 19 to 20.10.4-1.amzn2 have a bug that prevents docker compose to start containers
DOCKER_VERSION="18.09.9ce-2.amzn2"
DOCKER_COMPOSE_VERSION="1.29.2"
yum install -y \
    gitlab-runner \
    gcc\
    openssl-devel \
    python3 \
    jq \
    tree \
    nodejs \
    docker-$DOCKER_VERSION

# Update pip
python3 -m pip install -U pip

# Install docker-compose
pip3 install docker-compose==$DOCKER_COMPOSE_VERSION

#-------------------------------------------------------------------------------
# Gitlab runner configuration
#-------------------------------------------------------------------------------
# Create gitlab-runner user so we can have a fixed UID/GID. 
# Prevents file permission issues on restarts.
GITLAB_USER="gitlab-runner"
GITLAB_HOME="/home/$GITLAB_USER"
GITLAB_SSH="$GITLAB_HOME/.ssh"
groupmod -g 100000 $GITLAB_USER
usermod -u 100000 -g $GITLAB_USER -s /bin/bash -c "$GITLAB_USER" "$GITLAB_USER"

# Ensure all files created by root inside docker belong to $GITLAB_USER rather than root. 
# This helps with file permission issue e.g. stashing files.
# subuid
echo "$GITLAB_USER:100000:1"      >> /etc/subuid
echo "$GITLAB_USER:1000000:65536" >> /etc/subuid
# subgid
echo "$GITLAB_USER:100000:1"      >> /etc/subgid
echo "$GITLAB_USER:1000000:65536" >> /etc/subgid

cat <<EOF >> /etc/docker/daemon.json
{
    "userns-remap": "$GITLAB_USER"
}
EOF

# Modifying hop limit due to know aws provider issue
# See: https://github.com/terraform-providers/terraform-provider-aws/issues/10949
INSTANCE_ID=`curl http://169.254.169.254/latest/meta-data/instance-id`
aws ec2 modify-instance-metadata-options --instance-id $INSTANCE_ID --http-put-response-hop-limit 3 --region ${aws_region} --http-endpoint enabled

# Get GitLab Token
GITLAB_TOKEN=`aws ssm get-parameters --region ${aws_region} --name ${token_ssm_param} --with-decryption --query "Parameters[*].{Value:Value}" --output text`

# Setting concurrent gitlab runners
sed -i "s/^concurrent =.*/concurrent = ${concurrent}/g" /etc/gitlab-runner/config.toml

# Register Runner
if [ "${executor}" == "shell" ]; then
    gitlab-runner register \
        --non-interactive \
        --name "${runner_name}" \
        --url "${gitlab_https_url}" \
        --registration-token "$GITLAB_TOKEN" \
        --tag-list "${runner_tags}" \
        --executor "shell" \
        --shell "bash"
else
    gitlab-runner register \
        --non-interactive \
        --name "${runner_name}" \
        --url "${gitlab_https_url}" \
        --registration-token "$GITLAB_TOKEN" \
        --tag-list "${runner_tags}" \
        --executor "docker" \
        --docker-image "${docker_image}"
fi

# Update $GITLAB_USER files ownership
chown -R "$GITLAB_USER:$GITLAB_USER" $GITLAB_HOME

# Restart gitlab-runner
gitlab-runner restart

#-------------------------------------------------------------------------------
# Docker configuration
#-------------------------------------------------------------------------------
# Add $GITLAB_USER to docker group
usermod -aG docker $GITLAB_USER
chkconfig docker on
systemctl restart docker

#-------------------------------------------------------------------------------
# Time zone configuration
#-------------------------------------------------------------------------------
# Configure server time zone
ln -sf /usr/share/zoneinfo/${timezone} /etc/localtime
#-------------------------------------------------------------------------------
# Enable Swap
#-------------------------------------------------------------------------------
# Create Swap file and activate it
dd if=/dev/zero of=/swapfile bs=128M count=16
chmod 600 /swapfile
mkswap /swapfile
swapon /swapfile
swapon -s
echo "/swapfile    swap    swap    defaults    0    0" | sudo tee -a /etc/fstab
#-------------------------------------------------------------------------------
# Runner maintaince tasks
#-------------------------------------------------------------------------------
# Prune unsuded networks and volumes
sudo echo "15 3 * * * /usr/bin/docker network prune -f" >> /var/spool/cron/root
sudo echo "20 3 * * * /usr/bin/docker volume prune -f" >> /var/spool/cron/root