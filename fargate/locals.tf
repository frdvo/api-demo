locals {
  env = {

    demo-local = {
      aws_profile                                 = "default"
      aws_region                                  = "ap-southeast-2"
      name                                        = "api-demo-${random_id.name.hex}"
      name_prefix                                 = "api-demo-${random_id.name.hex}"
      vpc_cidr_block                              = "10.54.0.0/16"
      availability_zones                          = ["ap-southeast-2a", "ap-southeast-2b", "ap-southeast-2c"]
      public_subnets_cidrs_per_availability_zone  = ["10.54.101.0/24", "10.54.102.0/24", "10.54.103.0/24"]
      private_subnets_cidrs_per_availability_zone = ["10.54.201.0/24", "10.54.202.0/24", "10.54.203.0/24"]
      single_nat                                  = true
      container_name                              = "api-demo"
      container_cpu                               = "256"
      container_memory                            = "512"
      container_memory_reservation                = "512"
      log_configuration = {
        "logDriver" : "awslogs",
        "options" : {
          "awslogs-group" : "api-demo-${random_id.name.hex}",
          "awslogs-region" : "ap-southeast-2",
          "awslogs-stream-prefix" : "api-demo"
        }
      }
      log_retention_days = 7
      environment = [{
        name  = "GIT_HASH"
        value = var.git_hash
      }]
      lb_http_ports           = local.lb_http_redirect_to_https
      lb_https_ports          = local.lb_https_forward_to_http
      default_certificate_arn = "arn:aws:acm:ap-southeast-2:722141136946:certificate/ee16de7e-cb05-4897-bbab-3737c5e2008c"
      desired_count           = 1
      enable_stick_session    = false
      tags = {
        Name       = "api-demo"
        repository = "api-demo/fargate"
        workspace  = terraform.workspace
      }
    }

    demo-shared = {
      aws_profile                                 = "demo-shared"
      aws_region                                  = "ap-southeast-2"
      name                                        = "api-demo-${random_id.name.hex}"
      name_prefix                                 = "api-demo-${random_id.name.hex}"
      vpc_cidr_block                              = "10.81.0.0/16"
      availability_zones                          = ["ap-southeast-2a", "ap-southeast-2b", "ap-southeast-2c"]
      public_subnets_cidrs_per_availability_zone  = ["10.81.101.0/24", "10.81.102.0/24", "10.81.103.0/24"]
      private_subnets_cidrs_per_availability_zone = ["10.81.201.0/24", "10.81.202.0/24", "10.81.203.0/24"]
      single_nat                                  = true
      container_name                              = "api-demo"
      container_cpu                               = "256"
      container_memory                            = "512"
      container_memory_reservation                = "512"
      log_configuration = {
        "logDriver" : "awslogs",
        "options" : {
          "awslogs-group" : "api-demo-${random_id.name.hex}",
          "awslogs-region" : "ap-southeast-2",
          "awslogs-stream-prefix" : "api-demo"
        }
      }
      log_retention_days = 7
      environment = [{
        name  = "GIT_HASH"
        value = var.git_hash
      }]
      lb_http_ports           = local.lb_http_redirect_to_https
      lb_https_ports          = local.lb_https_forward_to_http
      default_certificate_arn = "arn:aws:acm:ap-southeast-2:722141136946:certificate/ee16de7e-cb05-4897-bbab-3737c5e2008c"
      desired_count           = 1
      enable_stick_session    = false
      tags = {
        Name       = "api-demo"
        repository = "api-demo/fargate"
        workspace  = terraform.workspace
      }
    }

    demo-dev = {
      aws_profile                                 = "demo-dev"
      aws_region                                  = "ap-southeast-2"
      name                                        = "api-demo-${random_id.name.hex}"
      name_prefix                                 = "api-demo-${random_id.name.hex}"
      vpc_cidr_block                              = "10.56.0.0/16"
      availability_zones                          = ["ap-southeast-2a", "ap-southeast-2b", "ap-southeast-2c"]
      public_subnets_cidrs_per_availability_zone  = ["10.56.101.0/24", "10.56.102.0/24", "10.56.103.0/24"]
      private_subnets_cidrs_per_availability_zone = ["10.56.201.0/24", "10.56.202.0/24", "10.56.203.0/24"]
      single_nat                                  = true
      container_name                              = "api-demo"
      container_cpu                               = "256"
      container_memory                            = "512"
      container_memory_reservation                = "512"
      log_configuration = {
        "logDriver" : "awslogs",
        "options" : {
          "awslogs-group" : "api-demo-${random_id.name.hex}",
          "awslogs-region" : "ap-southeast-2",
          "awslogs-stream-prefix" : "api-demo"
        }
      }
      log_retention_days = 7
      environment = [{
        name  = "GIT_HASH"
        value = var.git_hash
      }]
      lb_http_ports           = local.lb_http_forward_to_http
      lb_https_ports          = local.lb_https_disabled
      desired_count           = 1
      enable_stick_session    = false
      tags = {
        Name       = "api-demo"
        repository = "api-demo/fargate"
        workspace  = terraform.workspace
      }
    }
 

  }




  workspace = local.env[terraform.workspace]

  ## Load Balancer Shared Settings

  lb_https_disabled = {}
  lb_http_forward_to_http = {
    "default_http" : {
      "listener_port" : 80,
      "target_group_port" : 80,
      "type" : "forward"
    }
  }

  lb_http_disabled = {}
  lb_http_redirect_to_https = {
    "default_http" : {
      "type" : "redirect",
      "listener_port" : 80,
      "host" : "#{host}",
      "path" : "/#{path}",
      "port" : 443,
      "protocol" : "HTTPS",
      "query" : "#{query}",
      "status_code" : "HTTP_301"
  } }
  lb_https_forward_to_http = {
    "default_https" : {
      "listener_port" : 443,
      "target_group_port" : 80,
      "type" : "forward"
      "target_group_protocol" : "HTTP"
    }
  }
}
