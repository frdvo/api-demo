module "base-network" {
  source                                      = "cn-terraform/networking/aws"
  version                                     = "2.0.13"
  name_prefix                                 = local.workspace.name_prefix
  vpc_cidr_block                              = local.workspace.vpc_cidr_block
  availability_zones                          = local.workspace.availability_zones
  public_subnets_cidrs_per_availability_zone  = local.workspace.public_subnets_cidrs_per_availability_zone
  private_subnets_cidrs_per_availability_zone = local.workspace.private_subnets_cidrs_per_availability_zone
  single_nat                                  = local.workspace.single_nat
}