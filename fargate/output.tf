output "health_api_url" {
  value = "http://${module.service.aws_lb_lb_dns_name}/health"
}