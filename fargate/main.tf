module "cluster" {
  source  = "cn-terraform/ecs-cluster/aws"
  version = "1.0.7"
  name    = local.workspace.name
}

module "td" {
  source                       = "cn-terraform/ecs-fargate-task-definition/aws"
  version                      = "1.0.24"
  name_prefix                  = local.workspace.name_prefix
  container_image              = var.container_url
  container_name               = local.workspace.container_name
  container_cpu                = local.workspace.container_cpu
  container_memory             = local.workspace.container_memory
  container_memory_reservation = local.workspace.container_memory_reservation
  log_configuration            = local.workspace.log_configuration
  environment                  = local.workspace.environment
  port_mappings = [{ 
                      containerPort = 80
                      protocol = " tcp"
                      hostPort = 80
                      }]
}

module "service" {
  source                  = "cn-terraform/ecs-fargate-service/aws"
  version                 = "2.0.18"
  name_prefix             = local.workspace.name_prefix
  vpc_id                  = module.base-network.vpc_id
  ecs_cluster_arn         = module.cluster.aws_ecs_cluster_cluster_arn
  task_definition_arn     = module.td.aws_ecs_task_definition_td_arn
  public_subnets          = module.base-network.public_subnets_ids
  private_subnets         = module.base-network.private_subnets_ids
  container_name          = local.workspace.container_name
  ecs_cluster_name        = module.cluster.aws_ecs_cluster_cluster_name
  lb_http_ports           = local.workspace.lb_http_ports
  lb_https_ports          = local.workspace.lb_https_ports
  default_certificate_arn = try(local.workspace.default_certificate_arn, null)
  desired_count           = local.workspace.desired_count
  lb_enable_http2         = true
  lb_stickiness = {
                    "cookie_duration" : 86400,
                    "enabled" : local.workspace.enable_stick_session,
                    "type" : "lb_cookie"
  }

}
