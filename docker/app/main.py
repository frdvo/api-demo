# Versioning

title = "api-demo" # app_name
description = "Health Demo API"
version = "0.1" # app_version

# App itself

import os
from fastapi import FastAPI


git_hash = os.environ.get('GIT_HASH')
hostname = os.environ.get('HOSTNAME')
printenv = os.environ

app = FastAPI()


@app.get("/health")
async def health():
    return {
        "app_name": title,
        "app_version": version,
        "git_hash": git_hash,
     }

@app.get("/")
async def root():
    return {
        "Hi there! Check the API at /health"
     }

# Playgroud

@app.get("/hostname")
async def health():
    return {
        "git_hash": git_hash,
        "hostname": hostname
     }

@app.get("/printenv")
async def health():
    return {
        "vars": printenv
     }